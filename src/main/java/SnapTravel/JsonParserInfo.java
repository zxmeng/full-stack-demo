package SnapTravel;

import java.util.*;
import java.lang.*;

import SnapTravel.Hotel;

public class JsonParserInfo {
    private List<Hotel> hotels;
    private HashMap<Long, Double> prices;

    JsonParserInfo(List<Hotel> hotels, HashMap<Long, Double> prices) {
    	this.hotels = hotels;
    	this.prices = prices;
    }

    public List<Hotel> getHotels() {return this.hotels;} 
	public void setHotels(List<Hotel> hotels) {this.hotels = hotels;}

	public HashMap<Long, Double> getPrices() {return this.prices;} 
	public void setPrices(HashMap<Long, Double> prices) {this.prices = prices;}

}