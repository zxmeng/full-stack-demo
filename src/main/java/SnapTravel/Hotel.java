package SnapTravel;

import java.util.*;
import java.lang.*;

public class Hotel {
    private Long hotelId;
    private String hotelName;
    private Long numReviews;
    private String address;
    private Integer numStars;
    private List<String> amenities;
    private String imageUrl;
    private Double stPrice;
    private Double rePrice;

    Hotel(Long hotelId, String hotelName, Long numReviews, String address, Integer numStars, List<String> amenities, String imageUrl, Double stPrice, Double rePrice) {
    	this.hotelId = hotelId;
    	this.hotelName = hotelName;
    	this.numReviews = numReviews;
    	this.address = address;
    	this.numStars = numStars;
        this.amenities = amenities;
        this.imageUrl = imageUrl;
        this.stPrice = stPrice;
        this.rePrice = rePrice;
    }

	public Long getHotelId() {return this.hotelId;} 
	public void setHotelId(Long hotelId) {this.hotelId = hotelId;}

	public String getHotelName() {return this.hotelName;} 
	public void setHotelName(String hotelName) {this.hotelName = hotelName;}

    public Long getNumReviews() {return this.numReviews;}
    public void setNumReviews(Long numReviews) {this.numReviews = numReviews;}

	public String getAddress() {return this.address;} 
	public void setAddress(String address) {this.address = address;}

    public Integer getNumStars() {return this.numStars;}
    public void setNumStars(Integer numStars) {this.numStars = numStars;}

	public List<String> getAmenities() {return this.amenities;} 
	public void setAmenities(List<String> amenities) {this.amenities = amenities;}

	public String getImageUrl() {return this.imageUrl;} 
	public void setImageUrl(String imageUrl) {this.imageUrl = imageUrl;}

    public Double getStPrice() {return this.stPrice;}
    public void setStPrice(Double stPrice) {this.stPrice = stPrice;}

    public Double getRePrice() {return this.rePrice;}
    public void setRePrice(Double rePrice) {this.rePrice = rePrice;}
}


