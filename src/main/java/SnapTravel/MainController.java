package SnapTravel;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.ui.Model;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.apache.commons.io.IOUtils;

import java.util.*;
import java.lang.*;
import java.io.*;
import java.net.*;
import java.nio.charset.Charset;

import SnapTravel.Hotel;
import SnapTravel.JsonParserInfo;

@Controller
public class MainController {

	@GetMapping("/home")
	public String runner(Model model, @RequestParam String stRes
		, @RequestParam String reRes) {

		// read json data
		JsonParserInfo response_st = jsonParserHotel(stRes);
		List<Hotel> hotels_st = response_st.getHotels();
		HashMap<Long, Double> prices_st = response_st.getPrices();

		JsonParserInfo response_re = jsonParserHotel(reRes);
		List<Hotel> hotels_re = response_re.getHotels();
		HashMap<Long, Double> prices_re = response_re.getPrices();

		hotels_st.retainAll(hotels_re);
		for (Hotel hotel: hotels_st) {
			hotel.setStPrice(prices_st.get(hotel.getHotelId()));
			hotel.setRePrice(prices_re.get(hotel.getHotelId()));
		}

		model.addAttribute("hotels", hotels_st);
		return "home";
	}

	private JsonParserInfo jsonParserHotel(String jsonStr) {
		JSONParser parser = new JSONParser();
		List<Hotel> hotels = new ArrayList<>();
		HashMap<Long, Double> prices = new HashMap<>();

		try {
			JSONArray jsonData = (JSONArray) parser.parse(jsonStr);

			for(Object o: jsonData) {
				// for each object, get field values
				JSONObject obj = (JSONObject) o;

				Long hotelId = (Long) obj.get("id");
				String hotelName = (String) obj.get("hotel_name");
				Long numReviews = (Long) obj.get("num_reviews");
				String address = (String) obj.get("address");
				Integer numStars = (Integer) obj.get("num_stars");
				List<String> amenities = (List<String>) obj.get("amenities");
				String imageUrl = (String) obj.get("image_url");
				Double price = (Double) obj.get("price");

				// construct Hotel object and add to list
				Hotel h = new Hotel(hotelId, hotelName, numReviews, address, numStars, amenities, imageUrl, 0.0, 0.0);
				hotels.add(h);
				prices.put(hotelId, price);
			}

		} catch (Exception e) {
			e.printStackTrace();
		}

		return new JsonParserInfo(hotels, prices);
	}

}