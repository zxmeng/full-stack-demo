function submit(){
	var city = document.getElementById('city').value;
	var checkin = document.getElementById('checkin').value;
	var checkout = document.getElementById('checkout').value;

	var params_s = "city=" + city + "&checkin=" + checkin + "&checkout=" + checkout + "&provider=snaptravel";
	var params_r = "city=" + city + "&checkin=" + checkin + "&checkout=" + checkout + "&provider=retail";

	var stRes = snapTravelQuest(params_s);
	var reRes = retailQuest(params_r);

	window.location.href = "/home?stRes=" + stRes + "&reRes=" + reRes;
}

function snapTravelQuest(params) {
	var http = new XMLHttpRequest();
	var url = 'https://experimentation.getsnaptravel.com/interview/hotels';
	http.open('POST', url, true);
	http.onreadystatechange = function() {
	    if (http.readyState == 4 && http.status == 200) {
	    	return http.responseText
	    }
	}
	http.send(params);
}

function retailQuest(params) {
	var http = new XMLHttpRequest();
	var url = 'https://experimentation.getsnaptravel.com/interview/hotels';
		http.open('POST', url, true);
	http.onreadystatechange = function() {
	    if (http.readyState == 4 && http.status == 200) {
	    	return http.responseText
	    }
	}
	http.send(params);
}